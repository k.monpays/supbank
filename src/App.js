import React, { Component } from "react";

import { Route, Switch } from "react-router-dom";
import "./App.css";
import {
    CSSTransition,
    TransitionGroup,
} from 'react-transition-group';
import NotFound from "./containers/NotFound";
import Login from "./containers/Login";
import Signup from "./containers/Signup";
import Wallet from "./containers/Wallet";
import Blockchain from "./containers/Blockchain";
import Frontpage from "./containers/Frontpage";
import Policies from "./containers/Policies";
import LearnMore from "./containers/LearnMore";
import AboutUs from "./containers/AboutUs";
import MyAccount from "./containers/MyAccount";
import Transactions from "./containers/Transactions";

class App extends Component {
    render() {
        return (
            <div className="App container">
                <Route render={({location}) => (

                    <TransitionGroup>
                        <CSSTransition
                            key={location.key}
                            timeout={450}
                            classNames="fade"
                        >
                            <Switch location={location}>
                                <Route path="/" exact component={Frontpage} />
                                <Route path="/frontpage" exact component={Frontpage} />
                                <Route path="/login" exact component={Login} />
                                <Route path="/signup" exact component={Signup}/>
                                <Route path="/wallet" exact component={Wallet}/>
                                <Route path="/blockchain" exact component={Blockchain}/>
                                <Route path="/policies" exact component={Policies}/>
                                <Route path="/learn-more" exact component={LearnMore}/>
                                <Route path="/about-us" exact component={AboutUs}/>
                                <Route path="/myaccount" exact component={MyAccount}/>
                                <Route path="/transactions" exact component={Transactions}/>
                                <Route component={NotFound} />
                            </Switch>
                        </CSSTransition>
                    </TransitionGroup>
                )} />

            </div>
        );
    }
}

export default App;