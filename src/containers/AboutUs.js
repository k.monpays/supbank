import React, { Component } from "react";
import "../styles/AboutUs.css";
import NavBar from "./NavBar";


export default class AboutUs extends Component {
    render() {
        return (
            <div className="WhoAreWe page">
                <div className="lander">
                    <NavBar />
                </div>

                <div className="content page-top">
                    <h1 className="page-title">About Us</h1>
                    <div className="description-line">
                        <div className="description inline-block">
                            <h1 className="description-title">The first French bank investing into cryptocurrency</h1>
                            <p className="description-text">Most of the people are not yet introduced to cryptocurrency. They may have heard about it, but not
                                that much. We are a team of students, bring part of Supbank, with the mission to make more accessible investing into cryptocurrency.
                                Mining is also a part of the blockchain technology, and is part of cryptocurrency. </p>
                        </div>

                        <img className="description-image inline-block" alt="Office" src="https://media.glassdoor.com/l/fc/08/7f/10/office.jpg"/>
                    </div>

                    <div className="team-members">
                        <h1 className="team-title">Our team</h1>
                        <div className="left-side team-height inline-block">
                            <div className="top-placed-member">
                                <img className="placed-member-image centered-text" alt="Pierre-Louis" src="https://www.campus-booster.net/actorpictures/281999.jpg"/>
                                <p className="placed-member-name centered-text">Pierre-Louis TALBOT</p>
                                <p className="placed-member-title centered-text">Project Leader/Back-end Developer</p>
                            </div>
                            <div className="bottom-placed-member">
                                <img className="placed-member-image centered-text" alt="Arnaud" src="https://www.campus-booster.net/actorpictures/214364.jpg" />
                                <p className="placed-member-name centered-text">Kévin MONPAYS</p>
                                <p className="placed-member-title centered-text">Front-end Developer</p>
                            </div>
                        </div>

                        <div className="middle-side team-height inline-block">
                            <div className="top-placed-member">
                                <img className="placed-member-image centered-text" alt="Solène" src="https://www.campus-booster.net/actorpictures/218047.jpg" />
                                <p className="placed-member-name centered-text">Solène PATRUNO</p>
                                <p className="placed-member-title centered-text">System Administrator</p>
                            </div>
                            <div className="bottom-placed-member">
                                <img className="placed-member-image centered-text" alt="Crédo" src="https://www.campus-booster.net/actorpictures/297505.jpg" />
                                <p className="placed-member-name centered-text">Crédo WELEKETI</p>
                                <p className="placed-member-title centered-text">BI Consultant</p>
                            </div>
                        </div>

                        <div className="right-side team-height inline-block">
                            <div className="top-placed-member">
                                <img className="placed-member-image centered-text" alt="Arnaud" src="https://www.campus-booster.net/actorpictures/214875.jpg" />
                                <p className="placed-member-name centered-text">Arnaud PERRIER</p>
                                <p className="placed-member-title centered-text">Mobile Developer</p>
                            </div>
                            <div className="bottom-placed-member">
                                <img className="placed-member-image centered-text" alt="Dihuer-Hermann" src="https://www.campus-booster.net/actorpictures/296651.jpg" />
                                <p className="placed-member-name centered-text">Dihuer-Hermann KOUADIO</p>
                                <p className="placed-member-title centered-text">Database Manager</p>
                            </div>
                        </div>
                    </div>

                    <div className="footer" style={{backgroundImage: 'url("/static/media/d1SOh5.3989e842.jpg")', backgroundPosition: "bottom", color: "white"}}>
                        <div className="brand inline-block">
                            <p>SupBank - © Copyright 2019</p>
                        </div>

                        <div className="footer-nav inline-block">
                            <a href="policies" style={{color: "white"}}>Privacy Policy</a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}