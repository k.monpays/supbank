import React, { Component } from "react";
import {Link} from "react-router-dom";
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

import "../styles/AppBar.css";


export default class AppBar extends Component {

    disconnectUser = () => {
        localStorage.removeItem('userID');
        localStorage.removeItem('userToken');
    };

    render() {
        return (
            <div className="AppBar container">
                <Navbar fluid collapseOnSelect className="navigation">
                    <Navbar.Header>
                        <Navbar.Brand className="logo">
                            <Link id="appbar-logo" to="/">SupBank</Link>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                        <Nav pullRight id="appbar-navigation-items" className="navigation-items">
                            <LinkContainer to="/wallet" className="appbar-navigation-item-unique" >
                                <NavItem >Wallet</NavItem>
                            </LinkContainer>
                            <LinkContainer to="/blockchain" className="appbar-navigation-item-unique" >
                                <NavItem >Blockchain</NavItem>
                            </LinkContainer>
                            <LinkContainer to="/transactions" className="appbar-navigation-item-unique" >
                                <NavItem >Transactions</NavItem>
                            </LinkContainer>
                        </Nav>
                    </Navbar.Header>
                    <Navbar.Collapse className="collapse-right">
                        <Nav pullRight className="user-actions-logged">
                            <LinkContainer to="/myaccount" className="appbar-navigation-item-unique" >
                                <NavItem >My Account</NavItem>
                            </LinkContainer>
                            <LinkContainer to="/frontpage" className="appbar-navigation-item-unique" >
                                <NavItem onClick={this.disconnectUser}>Disconnect</NavItem>
                            </LinkContainer>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>

            </div>
        )
    }
}