import React, { Component } from "react";
import AppBar from "./AppBar";
import "../styles/Blockchain.css";
import axios from "axios";

export default class Blockchain extends Component {

    constructor(props) {
        super(props);

        /*if(localStorage.getItem('userID') === null || localStorage.getItem('userID') === undefined ){
            this.props.history.push('/frontpage');
        }*/

        this.state = {
            Block1Number: "",
            Block1Timestamp: "",
            Block1PrevHash: "",
            Block1EndHash: "",
            Block1Nonce: "",
            Block1NbTransactions: "",
            Block2Number: "",
            Block2Timestamp: "",
            Block2PrevHash: "",
            Block2EndHash: "",
            Block2Nonce: "",
            Block2NbTransactions: "",
            Block3Number: "",
            Block3Timestamp: "",
            Block3PrevHash: "",
            Block3EndHash: "",
            Block3Nonce: "",
            Block3NbTransactions: "",
            BlockchainLength: ""
        };

        this.getBlockchainLength((res) =>{
            this.setState({
                BlockchainLength: res.data.length,
            });
            this.getInitialBlockInfo();
        });
    }

    getInitialBlockInfo(res) {

        this.getFirstBlockInfo((res) => {
            res.data.timestamp = res.data.timestamp.replace("T", " ");
            this.setState({
                Block1Number: res.data.id,
                Block1Timestamp: res.data.timestamp.substr(0, 19),
                Block1PrevHash: res.data.previoushash,
                Block1EndHash: res.data.hash,
                Block1Nonce: res.data.nonce,
                Block1NbTransactions: res.data.transactions.length
            });
        });


        this.getSecondBlockInfo((res) => {
            res.data.timestamp = res.data.timestamp.replace("T", " ");

            this.setState({
                Block2Number: res.data.id,
                Block2Timestamp: res.data.timestamp.substr(0, 19),
                Block2PrevHash: res.data.previoushash,
                Block2EndHash: res.data.hash,
                Block2Nonce: res.data.nonce,
                Block2NbTransactions: res.data.transactions.length
            })
        });

        this.getThirdBlockInfo((res) => {
            res.data.timestamp = res.data.timestamp.replace("T", " ");
            this.setState({
                Block3Number: res.data.id,
                Block3Timestamp: res.data.timestamp.substr(0, 19),
                Block3PrevHash: res.data.previoushash,
                Block3EndHash: res.data.hash,
                Block3Nonce: res.data.nonce,
                Block3NbTransactions: res.data.transactions.length
            })
        });
    };

    getBlockchainLength(res) {
        axios({
            method: 'get',
            url: 'https://api-supbank.ddns.net/blockchain',
            headers: {
                'Authorization': localStorage.getItem('userToken')
            }
        })
            .then(function (response) {
                console.log(response);
                const success = response.data.success;
                if(success) {
                    console.log(success);
                }
                res(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    getFirstBlockInfo(res) {
        axios({
            method: 'get',
            url: 'https://api-supbank.ddns.net/blockchain/' + (this.state.BlockchainLength - 2),
            headers: {
                'Authorization': localStorage.getItem('userToken')
            }
        })
            .then(function (response) {
                console.log(response);
                const success = response.data.success;
                if(success) {
                    console.log(success);
                }
                res(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    getSecondBlockInfo(res) {
        axios({
            method: 'get',
            url: 'https://api-supbank.ddns.net/blockchain/' + (this.state.BlockchainLength - 1),
            headers: {
                'Authorization': localStorage.getItem('userToken')
            }
        })
            .then(function (response) {
                console.log(response);
                const success = response.data.success;
                if(success) {
                    console.log(success);
                }
                res(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    getThirdBlockInfo(res) {
        axios({
            method: 'get',
            url: 'https://api-supbank.ddns.net/blockchain/' + this.state.BlockchainLength,
            headers: {
                'Authorization': localStorage.getItem('userToken')
            }
        })
            .then(function (response) {
                console.log(response);
                const success = response.data.success;
                if(success) {
                    console.log(success);
                }
                res(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        return (
            <div className="Blockchain page">
                <AppBar />
                <div id="blockchain-content">
                    <div id="Block-1">
                        <div>
                            <h3 className="Block-title">Block n°{this.state.Block1Number}</h3>
                            <p className="Block-timestamp">{this.state.Block1Timestamp}</p>
                        </div>
                        <div>
                            <p className="previous-hash-title Block-small-title">Previous Hash:</p>
                            <p className="hash-text prev-hash">{this.state.Block1PrevHash}</p>
                        </div>
                        <div>
                            <p className="Block-small-title">Data:</p>
                            <p className="Block-very-small-title">Number of transactions: {this.state.Block1NbTransactions}</p>
                            <p className="Block-very-small-title">Nonce: {this.state.Block1Nonce}</p>
                        </div>
                        <div>
                            <p className=" Block-small-title">End Hash :</p>
                            <p className="hash-text">{this.state.Block1EndHash}</p>

                        </div>
                    </div>

                    <div id="Block-2">
                        <div>
                            <h3 className="Block-title">Block n°{this.state.Block2Number}</h3>
                            <p className="Block-timestamp">{this.state.Block2Timestamp}</p>
                        </div>
                        <div>
                            <p className="previous-hash-title Block-small-title">Previous Hash:</p>
                            <p className="hash-text prev-hash">{this.state.Block2PrevHash}</p>
                        </div>
                        <div>
                            <p className="Block-small-title">Data:</p>
                            <p className="Block-very-small-title">Number of transactions: {this.state.Block2NbTransactions}</p>
                            <p className="Block-very-small-title">Nonce: {this.state.Block2Nonce}</p>
                        </div>
                        <div>
                            <p className="Block-3-secure-key Block-small-title">End Hash :</p>
                            <p className="hash-text">{this.state.Block2EndHash}</p>

                        </div>
                    </div>

                    <div id="Block-3">
                        <div>
                            <h3 className="Block-title">Block n°{this.state.Block3Number}</h3>
                            <p className="Block-timestamp">{this.state.Block3Timestamp}</p>
                        </div>
                        <div>
                            <p className="previous-hash-title Block-small-title">Previous Hash:</p>
                            <p className="hash-text prev-hash">{this.state.Block3PrevHash}</p>
                        </div>
                        <div>
                            <p className="Block-small-title">Data:</p>
                            <p className="Block-very-small-title">Number of transactions: {this.state.Block3NbTransactions}</p>
                            <p className="Block-very-small-title">Nonce: {this.state.Block3Nonce}</p></div>
                        <div>
                            <p className="Block-small-title">End Hash :</p>
                            <p className="hash-text">{this.state.Block3EndHash}</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
