import React, { Component } from "react";
import Autoplay from "./slider/Autoplay";
import {isBrowser, isMobile} from "react-device-detect";
import "../styles/Frontpage.css";
import NavBar from "./NavBar";
export default class Frontpage extends Component {

    renderContent = () => {
        if (isMobile) {
            return (
                <div className="Frontpage-mobile">
                    <div className="Frontpage-mobile-content">
                        <div className="mobile-logo-container">
                            <img src={require('../images/logo_plak.png')} alt={"Plaksium logo"} className="mobile-app-logo"/>
                        </div>
                        <p className="centered-text mobile-text-title">SupBank</p>
                        <div className="mobile-first-para">
                            <p className="centered-text mobile-text mobile-first-text">Our website is not available on mobile yet...</p>
                            <p className="centered-text mobile-text mobile-first-text">But we got an iOS app for you to manage you wallet!</p>
                        </div>
                        <div className="mobile-app-image">
                            <img src={require('../images/iOS-1024.png')} alt={"iOS icon"} className="mobile-app-image"/>
                            <p className="centered-text mobile-download-text">This app will be available soon...</p>
                        </div>
                            <p className="centered-text mobile-text mobile-message-computer">To access to the full website, visit us from a computer.</p>
                            <div className="footer-mobile-container">
                            <div className="footer-mobile">
                                <p className="copyright-mobile">SupBank - © Copyright 2019</p>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }

        if (isBrowser) {
            return (
                <div className="Frontpage page">
                    <div className="lander">
                        <NavBar />
                    </div>
                    <div className="content">
                        <div className="info">
                            <div className="slider-content">
                                <Autoplay />
                            </div>
                            <div className="footerContainer">
                                <div className="footer" style={{bottom: "-25px", backgroundImage: "none"} }>
                                    <div className="brand inline-block">
                                        <p>SupBank - © Copyright 2019</p>
                                    </div>

                                    <div className="footer-nav inline-block">
                                        <a href="policies">Privacy Policy</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
    render() {
        return this.renderContent();
    }
}
