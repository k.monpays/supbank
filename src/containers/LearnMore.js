import React, { Component } from "react";
import "../styles/LearnMore.css";
import NavBar from "./NavBar";


export default class LearnMore extends Component {
    render() {
        return (
            <div className="LearnMore page">
                <div className="lander">
                    <NavBar />
                </div>

                <div className="content learnmore-bg">
                    <div className="learn-one-line">
                       <div className="learn-right-image-line">
                           <h1 className="one-line-title">What is cryptocurrency?</h1>
                            <p className="learn-one-line-text learn-content">Cryptocurrency is a new medium of exchange that uses a strong cryptography in order
                                to secure transactions, identity and unit creation. It uses Blockchain, that takes the end of the last blocks hash to prove the formal
                                identity of the last block, preventing any changes to be done.</p>
                           <p className="learn-two-line-text learn-content">Transactions, wallets and mined tokens are safe, permitting the user to use our wallet and our cryptocurrency
                               safely. As most of the cryptocurrencies, it can be mined by users, giving them a way to earn tokens by the
                               power of their computers. Mining gets the economy running, as user transactions. As the money grows,
                               it will be available on more and more websites for your online purchases with our cryptocurrency.
                           </p>
                           <img className="learn-one-image" src={require("../images/blockchain.png")} alt="Hand with coin"/>
                       </div>
                    </div>
                    <div className="three-items-line">
                        <div id="learn-wallet-content" className="learn-container">
                            <img className="learn-image" src={require("../images/hand.png")} alt="Hand with coin"/>
                            <h3 className="learn-title">Mine to win</h3>
                            <p className="learn-content">If you do not want to directly invest money into your wallet, you have the possibility to mine
                                Plaksium. Mining uses your computer's power in order to try to calculate algorithms that values tokens. The more power you have,
                                the faster you will mine cryptocurrency.</p>
                        </div>

                        <div className="learn-blockchain-content learn-container">
                            <img className="learn-image" src={require("../images/locked-padlock.png")} alt="Hand with coin"/>
                            <h3 className="learn-title">Secured</h3>
                            <p className="learn-content">The Blockchain has been made in a way to get its data entirely secured.
                                The end of a block works like a securing key for the next content, taking away the
                                possibility to modify earlier blocks. Transactions, data and mined tokens are written
                                into the blocks, taking away the possibility for them to be modified.</p>
                        </div>

                        <div className="learn-charts-content learn-container">
                            <img className="learn-image" src={require("../images/clipboard-with-a-list.png")} alt="Hand with coin"/>
                            <h3 className="learn-title">Better decisions</h3>
                            <p className="learn-content">We offer a live chart graph of our cryptocurrency, permitting to users to get
                                a clear sight of up and downs of the money. With this tool, users have more information
                                to take the decision to buy or to sell tokens. These charts are working in the same way
                                as usual stock charts, but are, for now, focussed on our currency, PLK.</p>
                        </div>
                    </div>

                    <div className="footer">
                        <div className="brand inline-block" style={{color: "white"}}>
                            <p>SupBank - © Copyright 2019</p>
                        </div>

                        <div className="footer-nav inline-block">
                            <a href="policies" style={{color: "white"}}>Privacy Policy</a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

