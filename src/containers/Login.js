import React, { Component } from "react";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import axios from 'axios';
import NavBar from "./NavBar";
import "../styles/Login.css";

export default class Login extends Component {

    constructor(props) {
        super(props);

        if(localStorage.getItem('userID') !== null && localStorage.getItem('userID') !== undefined ){
            this.props.history.push('/wallet')
        }

        this.state = {
            email: "",
            password: "",
            loginError: false
        };
    }

    tryConnect(res) {
        axios({
            method: 'post',
            url: 'https://api-supbank.ddns.net/auth',
            data: {
                email: this.state.email,
                password: this.state.password
            }
        })
            .then((response) => {
                console.log(response);
                const success = response.data.success;
                if(success) {
                    localStorage.setItem('userID', response.data.userId);
                    localStorage.setItem('userToken', response.data.token);
                }

                res(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = event => {
        event.preventDefault();
        this.tryConnect((res) => {
            if(res.data.success === true) {
                this.props.history.push('/wallet')
            }

            else if (res.data.success === false){
                this.setState({
                    loginError: true
                });
            }
        });

    }

    render() {
        return (
            <div className="Login page">
                <NavBar />
                <h1 className="form-title"> Login </h1>
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="email" bsSize="large">
                        <ControlLabel>Email</ControlLabel>
                        <FormControl
                            autoFocus
                            type="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                            value={this.state.password}
                            onChange={this.handleChange}
                            type="password"
                        />
                    </FormGroup>
                    <LoaderButton
                        block
                        bsSize="large"
                        disabled={!this.validateForm()}
                        type="submit"
                        isLoading={this.state.isLoading}
                        text="Login"
                        loadingText="Logging in…"
                        id="send-button-login"
                    />
                </form>
                <p className={this.state.loginError ? "Login-Error" : "hidden Login-Error"}>Wrong credentials...</p>
            </div>
        );
    }
}
