import React, { Component } from "react";
import "../styles/MyAccount.css";
import AppBar from "./AppBar";
import axios from "axios";


export default class MyAccount extends Component {
    constructor(props) {
        super(props);

        /*if(localStorage.getItem('userID') === null || localStorage.getItem('userID') === undefined ){
            this.props.history.push('/frontpage');
        }*/

        this.state = {
            firstname: "",
            lastname: "",
            email: "",
            birthdate: "",
            publicwalletkey: "",
        };

        this.fetchUserInformation((res) => {
            this.setState({
                firstname: res.data.firstname,
                lastname: res.data.lastname,
                email: res.data.email,
                birthdate: res.data.birthdate.substring(0, 10),
                publicwalletkey: res.data.publicwalletkey
            })
        });

    }

    fetchUserInformation(res) {
        axios({
            method: 'get',
            url: 'https://api-supbank.ddns.net/user/' + localStorage.getItem('userID'),
            headers: {
                'Authorization': localStorage.getItem('userToken')
            }
        })
            .then(function (response) {
                console.log(response);
                const success = response.data.success;
                if(success) {
                    console.log(success);
                }
                res(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        return (
            <div className="MyAccount page">
                <div className="lander">
                    <AppBar />
                </div>

                <div className="myaccount-content">
                    <div className="myaccount-user-information">
                        <h2 className="myaccount-title">Hello {this.state.firstname} {this.state.lastname}, here is everything about your account:</h2>
                        <div className="myaccount-wallet">
                            <p className="myaccount-small-title myaccount-walletkey-title">Wallet Key:</p>
                            <p className="myaccount-content myaccount-walletkey">{this.state.publicwalletkey}</p>
                        </div>
                        <div className="myaccount-birthdate">
                            <p className="myaccount-small-title">Birthdate:</p>
                            <p className="myaccount-content">{this.state.birthdate}</p>
                        </div>
                        <div className="myaccount-email">
                            <p className="myaccount-small-title">Email:</p>
                            <p className="myaccount-content"> {this.state.email}</p>
                        </div>
                        <div className="myaccount-password">
                            <p className="myaccount-small-title">Change Password button</p>
                        </div>
                    </div>

                    <div className="myaccount-user-transactions">

                    </div>
                </div>
            </div>
        );
    }
}
