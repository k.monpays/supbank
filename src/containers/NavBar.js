import React, { Component } from "react";
import {Link} from "react-router-dom";
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import "../styles/NavBar.css";

export default class NavBar extends Component {
    render() {
        return (
            <div className="NavBar container">
                <Navbar fluid collapseOnSelect className="navigation">
                    <Navbar.Header>
                        <Navbar.Brand className="logo">
                            <Link to="/">SupBank</Link>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                        <Nav pullRight className="navigation-items">
                            <LinkContainer to="/frontpage" className="navigation-item-unique" >
                                <NavItem >Home</NavItem>
                            </LinkContainer>
                            <LinkContainer to="/learn-more" className="navigation-item-unique" >
                                <NavItem >Learn more</NavItem>
                            </LinkContainer>
                            <LinkContainer to="/about-us" className="navigation-item-unique" >
                                <NavItem >About Us</NavItem>
                            </LinkContainer>
                        </Nav>
                    </Navbar.Header>
                    <Navbar.Collapse>
                        <Nav pullRight className="user-actions">
                            <LinkContainer to="/signup" className="navigation-item-unique">
                                <NavItem>Signup</NavItem>
                            </LinkContainer>
                            <LinkContainer to="/login" className="navigation-item-unique">
                                <NavItem>Login</NavItem>
                            </LinkContainer>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </div>
        )
    }
}