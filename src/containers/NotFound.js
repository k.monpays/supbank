import React from "react";
import "../styles/NotFound.css";
import NavBar from "./NavBar";

export default () =>
    <div className="Error404 page">
        <NavBar />
        <div className="NotFound">
            <h3>Sorry, page not found!</h3>
        </div>;
    </div>
