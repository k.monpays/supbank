import React, { Component } from "react";
import "../styles/Policies.css";
import NavBar from "./NavBar";

export default class Policies extends Component {
    render() {
        return (
            <div className="Policies page">
                <NavBar />
                    <h2 className="Policy-title centered-text">Privacy policy</h2>

                    <p className="Policy-content centered-text">Politique Données personnelles du site internet
                        Garantir la sécurité et la confidentialité de vos données

                        La société X (Ci-après « la Société ») est soucieuse de la protection de votre vie privée et entend assurer une protection des données personnelles qui lui sont confiées. En effet, pour assurer le fonctionnement du site Internet accessible à l’adresse pp-supbank.ddns.net ainsi que de l’application dédiée (désignés collectivement par le « Site Internet ») édités par la société X cette dernière est amenée à collecter un certain nombre de données personnelles (ci-après la(les) « Donnée(s) »).

                        La présente politique a pour but de vous dispenser le maximum d’informations sur la façon dont vos données personnelles seront traitées dans le respect de la loi n°78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés dans sa version modifiée ainsi que dans le respect des autres textes français et communautaires applicables et notamment du RGPD (Règlement européen du 27 avril 2016).
                        Durée de conservation de vos données

                        Vos données personnelles sont conservées pour une durée selon les finalités de la collecte, à savoir :

                        Prospection commerciale pour 3 ans à compter du dernier contact ou du dernier achat
                        10 ans au titre de nos obligations légales et contractuelles
                        Lutte contre la fraude en magasin dans les conditions de prescription légale
                        Comment contacter la Société pour exercer vos droits ? </p>

                    <p className="Policy-content centered-text">Conformément au Règlement Européen sur la protection des données vous bénéficiez d’un droit d’accès, d’opposition au traitement, de modification (rectification, mise à jour), de suppression, de retrait du consentement, d’un droit à la limitation et à la portabilité de vos Données personnelles et pourrez définir des directives post-mortem. Tout exercice des droits devra être accompagné d’une copie de votre carte d’identité. Vous pourrez également introduire toute réclamation auprès d’une autorité de contrôle telle que la Commission Nationale Informatique et Libertés www.cnil.fr.

                        La Société respecte les règles applicables en fonction de chaque canal de prospection en droit français et en vertu du RGPD. Dans tous les cas, la Société vous donne la possibilité de vous y opposer soit par un lien de désabonnement présent dans la page, soit par le biais de votre compte, soit par sms en envoyant un stop sms.

                        Certaines Données non renseignées ou toute opposition à leur collecte peut rendre inaccessible certains services proposés sur le Site Internet.

                        Vous pouvez exercer vos droits en écrivant à l’adresse suivante XXXXXXXXXXXXXXXXXXXXXXXXXXXX
                        Vos Données personnelles sont hébergées au sein de l’Union Européenne.

                        Les Données collectées ont uniquement vocation à être utilisées par la Société.

                        La Société peut cependant faire appel à des prestataires sous-traitants commerciaux et techniques auxquels les Données pourraient être transmises de manière temporaire et sécurisée tels que :

                        Ceux qui les assistent et les aident à gérer des programmes de fidélité, la gestion de la relation client (CRM);
                        Leurs agences marketing pour réaliser leurs campagnes publicitaires, marketing et commerciales;
                        Ceux qui les assistent et les aident à fournir des services informatiques, pour la tenue de leurs bases de données et logiciels et applications corrélatifs : ces services peuvent parfois avoir accès à vos données pour réaliser les tâches demandées;

                        Des garanties ont été prises pour assurer un niveau de protection suffisant des Données.</p>

                    <p className="Policy-content last-content centered-text">Vos données sont susceptibles d’être transmises à des sous-traitants situés en dehors du territoire de l’Union Européenne.

                        Si le régime de protection d’un tel pays n’est pas considéré comme adéquat par la Commission européenne, une convention de flux transfrontière conforme aux clauses contractuelles types de la Commission européenne sera conclue afin d’encadrer le transfert des données et assurer un niveau de protection suffisant au regard des exigences de la réglementation française et européenne.

                        Vos Données pourraient être transmises également aux fins de répondre à une injonction des autorités légales.

                        En tout état de cause, les Données ne seront jamais transmises à des tiers dans un but commercial ni vendues ni échangées, sans votre consentement exprès.</p>

                <div className="footerContainer">
                    <div className="footer" style={{bottom: 0, backgroundImage: "none"} }>
                        <div className="brand inline-block">
                            <p>SupBank - © Copyright 2019</p>
                        </div>

                        <div className="footer-nav inline-block">
                            <a href="policies">Privacy Policy</a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}