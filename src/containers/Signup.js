import React, { Component } from "react";
import {
    FormGroup,
    FormControl,
    ControlLabel
} from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import NavBar from "./NavBar";
import "../styles/Signup.css";
import axios from "axios";

export default class Signup extends Component {
    constructor(props) {
        super(props);

        if(localStorage.getItem('userID') !== null && localStorage.getItem('userID') !== undefined ){
            this.props.history.push('/wallet')
        }

        this.state = {
            isLoading: false,
            firstname: "",
            lastname: "",
            birthdate: "",
            email: "",
            password: "",
            confirmPassword: "",
            signupError: false
        };
    }

    trySignup(res) {
        axios({
            method: 'post',
            url: 'https://api-supbank.ddns.net/user',
            data: {
                firstname: this.state.firstname,
                lastname: this.state.lastname,
                birthdate: this.state.birthdate,
                email: this.state.email,
                password: this.state.password
            }
        })
            .then(function (response) {
                console.log(response);
                const success = response.data.success;
                if(success) {

                }

                else {
                    this.setState({
                        isLoading: false,
                        signupError: true
                    });
                }
                res(response);
            })
            .catch(function (error) {
                console.log(error);

            });
    }

    componentDidMount() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear()-18;
        if(dd<10){
            dd='0'+dd
        }
        if(mm<10){
            mm='0'+mm
        }

        today = yyyy+'-'+mm+'-'+dd;
        document.getElementById("birthdate").setAttribute("max", today);
    }

    tryConnect(res) {
        axios({
            method: 'post',
            url: 'https://api-supbank.ddns.net/auth',
            data: {
                email: this.state.email,
                password: this.state.password
            }
        })
            .then((response) => {
                console.log(response);
                const success = response.data.success;
                if(success) {
                    localStorage.setItem('userID', response.data.userId);
                    localStorage.setItem('userToken', response.data.token);
                }

                res(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    validateForm() {
        return (
            this.state.email.length > 3 &&
            this.state.password.length > 5 &&
            this.state.firstname.length > 1 &&
            this.state.lastname.length > 1 &&
            this.state.password === this.state.confirmPassword
        );
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = async event => {
        event.preventDefault();
        this.setState({ isLoading: true });
        this.trySignup((res) => {
            if(res.data.success === true) {
                this.tryConnect((res2) => {
                    this.props.history.push('/wallet');
                    console.log(res2);
                });
            }

        });
    }

    renderForm() {
        return (
            <div className="Signup page">
                <NavBar />
                <h1 className="form-title"> Signup </h1>
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="firstname" className="col-xs-6 input" bsSize="large">
                        <ControlLabel>First Name</ControlLabel>
                        <FormControl
                            autoFocus
                            type="text"
                            value={this.state.firstname}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="lastname" className="col-xs-6 input2" bsSize="large">
                        <ControlLabel>Last Name</ControlLabel>
                        <FormControl
                            autoFocus
                            type="text"
                            value={this.state.lastname}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="birthdate" className="col-xs-6 input" bsSize="large">
                        <ControlLabel>Birth Date</ControlLabel>
                        <FormControl
                            autoFocus
                            type="date"
                            value={this.state.birthdate}
                            min={"1900-01-01"}

                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="email" className="col-xs-6 input2" bsSize="large">
                        <ControlLabel>Email</ControlLabel>
                        <FormControl
                            autoFocus
                            type="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                        />
                    </FormGroup>
                    <FormGroup controlId="password" className="input" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                            value={this.state.password}
                            onChange={this.handleChange}
                            type="password"
                        />
                    </FormGroup>
                    <FormGroup controlId="confirmPassword" className="input2" bsSize="large">
                        <ControlLabel>Confirm Password</ControlLabel>
                        <FormControl
                            value={this.state.confirmPassword}
                            onChange={this.handleChange}
                            type="password"
                        />
                    </FormGroup>
                    <LoaderButton
                        block
                        bsSize="large"
                        disabled={!this.validateForm()}
                        type="submit"
                        isLoading={this.state.isLoading}
                        text="Signup"
                        loadingText="Signing up…"
                        id="send-button-signup"
                    />
                </form>
                <p className={this.state.signupError ? "Login-Error" : "hidden Login-Error"}>Email already used...</p>
            </div>
        );
    }

    render() {
        return (
            <div>
                {this.renderForm()}
            </div>
        );
    }
}