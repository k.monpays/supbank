import React, { Component } from "react";
import "../styles/Transactions.css";
import AppBar from "./AppBar";
import axios from "axios";


export default class Transactions extends Component {
    constructor(props) {
        super(props);

        /*if(localStorage.getItem('userID') === null || localStorage.getItem('userID') === undefined ){
            this.props.history.push('/frontpage');
        }*/

        this.state = {
            userwalletkey: "",
            publickeywallet: "",
            walletamount: "",
            validwalletkey: false,
            validtransaction: false,
            errorwalletkey: false,
            amounterror: false,
            negativevalue: false,
            invalidwalletkey: false,
            errorownwalletkey: false,
            useramountentered: ""

        };

        this.getUserInfo( (res) => {
            this.setState({
                userwalletkey: res.data.publicwalletkey
            })
        });

        this.fetchUserBalance((res) => {
            this.setState({
                walletamount: res.data.amount,
            })
        });
    }

    publicWalletKeyChange = (e) => {
        this.setState({
            publickeywallet: e.target.value
        })
    }


    startTransaction(res) {
        axios({
            method: 'post',
            url: 'https://api-supbank.ddns.net/blockchain/transaction',
            headers: {
                'Authorization': localStorage.getItem('userToken')
            },
            data: {
                recipient: this.state.publickeywallet,
                amount: this.state.useramountentered
            }

        })
            .then(function (response) {
                res(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

     doesPublicWalletKeyExist(e) {
        e.preventDefault();
         this.setState({
             errorownwalletkey: false,
             validwalletkey: false,
             errorwalletkey: false
         });

        this.verifyPublicWalletKey((res) => {
            if(res.data.success) {
                if(this.state.publickeywallet === this.state.userwalletkey) {
                    this.setState({errorownwalletkey: true});
                }

                else {
                    this.setState({validwalletkey: true});
                }
            }
            else {
                this.setState({errorwalletkey: true});
            }

        });
    }

    verifyPublicWalletKey(res) {
        this.setState({publickeywallet: this.state.publickeywallet.replace(" ", "")});
        axios({
            method: 'get',
            url: 'https://api-supbank.ddns.net/user/find/wallet/' + this.state.publickeywallet,
            headers: {
                'Authorization': localStorage.getItem('userToken')
            }
        })
            .then((response) => {
                res(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    verifySendingBalance(e) {
        e.preventDefault();
        this.setState({
            useramountentered: this.state.useramountentered.replace(" ", ""),
            validtransaction: false,
            amounterror: false,
            negativevalue: false,
            invalidwalletkey: false
        });

        if(this.state.useramountentered > this.state.walletamount) {
            this.setState({amounterror: true});
        }

        else if (this.state.useramountentered < "0" || this.state.useramountentered.match(/^[0-9]+$/) == null) {
            this.setState({negativevalue: true});
        }

        else if (this.state.validwalletkey === false) {
            this.setState({invalidwalletkey: true});
        }

        else if (this.state.validwalletkey) {
            this.setState({
                validtransaction: true,
                walletamount: this.state.walletamount - this.state.useramountentered,
            });
            this.startTransaction();
        }

    }

    handleAmountChange = (e) => {
        this.setState({
            useramountentered: e.target.value
        })
    };


    getUserInfo(res) {
        axios({
            method: 'get',
            url: 'https://api-supbank.ddns.net/user/' + localStorage.getItem('userID'),
            headers: {
                'Authorization': localStorage.getItem('userToken')
            }
        })
            .then(function (response) {
                res(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    };

    fetchUserBalance(res) {
        axios({
            method: 'get',
            url: 'https://api-supbank.ddns.net/user/balance',
            headers: {
                'Authorization': localStorage.getItem('userToken')
            }
        })
            .then(function (response) {
                res(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    };

    render() {
        return (
            <div className="Transactions page">
                <div className="lander">
                    <AppBar />
                </div>

                <div className="transactions-title">
                    <h2>Are you here to send some tokens?</h2>
                </div>

                <div className="transactions-content">
                    <p className="inline-block">Enter a Public Wallet Key:</p>
                    <section className="section transaction-wallet-key-form inline-block">
                        <form className="form inline-block" id="verifyWalletPublicKey">
                            <input
                                type="text"
                                className="input search-wallet-key search-transactions"
                                id="searchInput"
                                placeholder="Wallet Public Key"
                                onChange={e => this.publicWalletKeyChange(e)}
                            />
                        </form>
                    </section>
                    <button className="inline-block submit-values" onClick={(e) => this.doesPublicWalletKeyExist(e)}>Verify</button>
                    <p className={this.state.errorwalletkey ? "Error-wallet-key inline-block" : "Error-wallet-key inline-block hidden"}>This Public Wallet Key doesn't exist.</p>
                    <p className={this.state.errorownwalletkey ? "Error-wallet-key inline-block" : "Error-wallet-key inline-block hidden"}>You cannot send money to yourself.</p>
                    <p className={this.state.validwalletkey ? "Good-wallet-key inline-block" : "Good-wallet-key inline-block hidden"}>Public Wallet Key found.</p>

                    <div className="transaction-sending">
                        <p className="transaction-sending-value inline-block">How many tokens do you want to send?</p>
                        <section className="section transaction-wallet-key-form inline-block">
                            <form className="form inline-block" id="verifyWalletPublicKey">
                                <input
                                    type="text"
                                    className="input tokens-amount search-transactions"
                                    id="token-amount"
                                    placeholder="Tokens"
                                    onChange={e => this.handleAmountChange(e)}
                                />
                                <button className="inline-block submit-values" onClick={(e) => this.verifySendingBalance(e)}>Send</button>
                            </form>
                            <p id="error-balance-low" className={this.state.amounterror ? "inline-block Error-wallet-key" : "inline-block hidden Error-wallet-key"}>Your token balance does not permit you that.</p>
                            <p id="error-amount-negative" className={this.state.negativevalue ? "inline-block Error-wallet-key" : "inline-block hidden Error-wallet-key"}>Negative/Empty value refused.</p>
                            <p id="error-amount-negative" className={this.state.invalidwalletkey ? "inline-block Error-wallet-key" : "inline-block hidden Error-wallet-key"}>Please verify the recipient public wallet key.</p>
                            <p id="error-amount-valid" className={this.state.validtransaction ? "inline-block Good-wallet-key" : "inline-block hidden Good-wallet-key"}>Transaction accepted.</p>
                        </section>
                        <p>You can send up to {this.state.walletamount} tokens.</p>
                    </div>
                </div>
            </div>
        );
    }
}
