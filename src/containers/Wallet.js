import React, { Component } from "react";
import AppBar from "./AppBar";
import "../styles/Wallet.css";
import axios from 'axios';
import $ from 'jquery';
import 'ajax';

export default class Wallet extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeButton: 'user',
            active: false,
            walletamount: '',
            searchBarValue: '',
            tokenValue: '',
            optionValue: ''
        };

        /*if(localStorage.getItem('userID') === null || localStorage.getItem('userID') === undefined ){
            this.props.history.push('/frontpage');
        }*/

        this.fetchUserBalance((res) => {
            this.setState({
                walletamount: res.data.amount,
            })
        });

        this.getTokenValue((res) => {
            this.setState({
                tokenValue: res.data.value,
            })
        });
        this.searchBarValueUpdate = this.searchBarValueUpdate.bind(this);
        this.optionValueUpdate = this.optionValueUpdate.bind(this);
    }

    fetchUserBalance(res) {
        axios({
            method: 'get',
            url: 'https://api-supbank.ddns.net/user/balance',
            headers: {
                'Authorization': localStorage.getItem('userToken')
            }
        })
            .then(function (response) {
                console.log(response);
                const success = response.data.success;
                if(success) {
                    console.log(success);
                }
                res(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    startMining(res) {

        axios({
            method: 'get',
            url: 'https://api-supbank.ddns.net/blockchain/mine',
            headers: {
                'Authorization': localStorage.getItem('userToken')
            }
        })
            .then(function (response) {
                console.log(response);
                const success = response.data.success;
                if(success) {
                    console.log(success);
                }
                res(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    getAPIWalletAddressTransaction (res){
        axios({
            method: 'get',
            url: 'https://api-supbank.ddns.net/blockchain/allTransaction',
            headers: {
                'Authorization': localStorage.getItem('userToken')
            }
        })
            .then((response) => {
                res(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    getTokenValue (res){
        axios({
            method: 'get',
            url: 'https://api-supbank.ddns.net/blockchain/value',
            headers: {
                'Authorization': localStorage.getItem('userToken')
            }
        })
            .then((response) => {
                res(response);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    searchBarValueUpdate (e) {
        this.setState({ searchBarValue: e.target.value });
    }

    optionValueUpdate (e) {
        this.setState({ optionValue: e.target.value });
    }

    handleSubmit = event => {
        event.preventDefault();
        this.getAPIWalletAddressTransaction((res) => {
            console.log(res);
            const transactions = res.data.pendingTransactions;

            res.data.savedTransactions.forEach((t) => {
                transactions.push(t);
            });

            var html = '';

            for(var i = 0; i < transactions.length; i++) {
                const t = transactions[i];
                if(this.state.optionValue === "wallet-address") {
                    if(t.sender !== this.state.searchBarValue && t.recipient !== this.state.searchBarValue) {
                        continue;
                    }
                }
                else if (this.state.optionValue === "block-identifier") {
                    t.blockId = '' + t.blockId;
                    if(t.blockId !== this.state.searchBarValue) {
                        continue;
                    }
                }

                if(!t.timestamp) {
                    t.timestamp = 'Pending...';
                    t.blockId = 'Pending...';
                }

                t.timestamp = t.timestamp.replace("T", " ");

                html += `
                    <div class="transaction-lines">
                        <div id="line-${ i + 1 }" class="transaction-line">
                            <p class="transaction-value transaction-title-timestamp inline-block">${ t.timestamp.substring(0,19) }</p>
                            <p class="transaction-value transaction-title-walletaddr inline-block">${ t.sender }</p>
                            <p class="transaction-value transaction-title-blockid inline-block">${ t.blockId }</p>
                            <p class="transaction-value transaction-title-amount inline-block">${ t.amount }</p>
                        </div>
                        <div class="transaction-line-advanced" id="search-line-${ i + 1 }-advanced">
                            <p class="transaction-sender inline-block">recipient: <span class="recipient-trans">${ t.recipient }</span></p>
                        </div>
                    </div>`;
            }

            $('#search-transactions-container-js').last().html(html);

            $(".transaction-line").click(function() {
                var contentPanelId = $(this).attr("id");
                var tempContentPanelId = 'search-' + contentPanelId + '-advanced';

                $('#'+tempContentPanelId).toggle();
            });
        });

    }

    componentDidMount() {
        function getUserTransactions(res) {
            axios({
                method: 'get',
                url: 'https://api-supbank.ddns.net/user/transaction',
                headers: {
                    'Authorization': localStorage.getItem('userToken')
                }
            })
                .then((response) => {
                    console.log(response);
                    res(response);
                })
                .catch(function (error) {
                    console.log(error);
                });
        }

        $(document).ready(function(event) {
            getUserTransactions((res) => {
                const transactions = res.data.pendingTransactions;

                res.data.savedTransactions.forEach((t) => {
                    transactions.push(t);
                });

                var html = '';

                for(var i = 0; i < transactions.length; i++) {
                    const t = transactions[i];

                    if(!t.timestamp) {
                        t.timestamp = 'Pending...';
                        t.blockId = 'Pending...';
                    }

                    t.timestamp = t.timestamp.replace("T", " ");

                    html += `
                    <div class="transaction-lines">
                        <div id="line-${ i + 1 }" class="transaction-line">
                            <p class="transaction-value transaction-title-timestamp inline-block">${ t.timestamp.substring(0,19) }</p>
                            <p class="transaction-value transaction-title-walletaddr inline-block">${ t.sender }</p>
                            <p class="transaction-value transaction-title-blockid inline-block">${ t.blockId }</p>
                            <p class="transaction-value transaction-title-amount inline-block">${ t.amount }</p>
                        </div>
                        <div class="transaction-line-advanced" id="user-line-${ i + 1 }-advanced">
                            <p class="transaction-sender inline-block">recipient: <span class="recipient-trans">${ t.recipient }</span></p>
                        </div>
                    </div>`;
                }

                $('#user-transactions-container-js').last().html(html);

                $(".transaction-line").click(function() {
                    var contentPanelId = $(this).attr("id");
                    var tempContentPanelId = 'user-' + contentPanelId + '-advanced';

                    $('#'+tempContentPanelId).toggle();
                });
            });

            function getAllTransactions(res) {
                axios({
                    method: 'get',
                    url: 'https://api-supbank.ddns.net/blockchain/allTransaction',
                    headers: {
                        'Authorization': localStorage.getItem('userToken')
                    }
                })
                    .then((response) => {
                        res(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }

            getAllTransactions((res) => {
                const transactions = res.data.pendingTransactions;

                res.data.savedTransactions.forEach((t) => {
                    transactions.push(t);
                });

                var html = '';

                for(var i = 0; i < transactions.length; i++) {
                    const t = transactions[i];

                    if(!t.timestamp) {
                        t.timestamp = 'Pending...';
                        t.blockId = 'Pending...';
                    }

                    t.timestamp = t.timestamp.replace("T", " ");

                    html += `
                    <div class="transaction-lines">
                        <div id="line-${ i + 1 }" class="transaction-line">
                            <p class="transaction-value transaction-title-timestamp inline-block">${ t.timestamp.substring(0,19) }</p>
                            <p class="transaction-value transaction-title-walletaddr inline-block">${ t.sender }</p>
                            <p class="transaction-value transaction-title-blockid inline-block">${ t.blockId }</p>
                            <p class="transaction-value transaction-title-amount inline-block">${ t.amount }</p>
                        </div>
                        <div class="transaction-line-advanced" id="global-line-${ i + 1 }-advanced">
                            <p class="transaction-sender inline-block">recipient: <span class="recipient-trans">${ t.recipient }</span></p>
                        </div>
                    </div>`;
                }

                $('#global-transactions-container-js').last().html(html);

                $(".transaction-line").click(function() {
                    var contentPanelId = $(this).attr("id");
                    var tempContentPanelId = 'global-' + contentPanelId + '-advanced';

                    $('#'+tempContentPanelId).toggle();
                });
            });
        });
    }

    toggleContent = (event) => {
        const currentState = this.state.active;
        event.preventDefault();
        this.setState({
            activeButton: event.target.value,
            active: !currentState
        })
    }

    render() {

        const { activeButton } = this.state;

        return (
            <div className="Wallet page">
                <AppBar />
                <div className="Wallet-content">
                    <h1>Welcome! </h1>
                    <h4 className="subtitle">Here is a recap of the last transactions: </h4>
                    <div id="information-line" className="inline-block">
                        <div className="live-charts inline-block">
                            <h3 id="rt-charts-title">Live value: </h3>
                            <p id="live-chart-value" className="chart-value">PLK - {this.state.tokenValue}</p>
                        </div>
                        <div className="user-token-ammount inline-block">
                            <h3 id="tokens-and-value-title">Wallet: </h3>
                            <p id="user-current-tokens-quantity" className="inline-block">Tokens: {this.state.walletamount}</p>
                        </div>
                        <button className="mine-button" onClick={this.startMining}>Click here to mine</button>
                    </div>

                    <div id="transactions">
                        <div id="global-transactions">
                            <button id="transaction-category-1" value="global" className={ activeButton ==='global' ? 'btn-active' : 'btn'} onClick={this.toggleContent}>Last Transactions</button>
                            <button id="transaction-category-2" value="user" className={ activeButton ==='user' ? 'btn-active' : 'btn'} onClick={this.toggleContent}>Your Transactions</button>
                            <button id="transaction-category-3" value="search" className={ activeButton ==='search' ? 'btn-active' : 'btn'} onClick={this.toggleContent}>Search Transactions</button>
                            <section className="section transaction-search-form inline-block">
                                <form className="form" id="addItemForm">
                                    <select id="dropdown-search-transactions" className="dropdown-search-transactions" onChange={this.optionValueUpdate}>
                                        <option value="wallet-address">Wallet address</option>
                                        <option value="block-identifier">Block Identifier</option>
                                    </select>
                                    <input
                                        type="text"
                                        className="input search-transactions"
                                        id="searchInput"
                                        placeholder="Search..."
                                        onChange={this.searchBarValueUpdate}
                                    />
                                    <button className="submit-values" onClick={this.handleSubmit}>Search</button>
                                </form>
                            </section>
                            <div id="global-transactions-container" className={ activeButton ==='global' ? 'transaction-container active' : 'transaction-container'}>
                                <div className="title-line">
                                    <div className="transaction-line-info">
                                        <p className="transaction-title transaction-title-timestamp inline-block">Timestamp</p>
                                        <p className="transaction-title transaction-title-walletaddr inline-block">Wallet address (sender)</p>
                                        <p className="transaction-title transaction-title-blockid inline-block">Block Identifier</p>
                                        <p className="transaction-title transaction-title-amount inline-block">Amount</p>
                                    </div>
                                </div>
                                <div id="global-transactions-container-js">
                                    <p>No transactions yet...</p>
                                </div>
                            </div>
                            <div id="user-transactions-container" className={ activeButton ==='user' ? 'transaction-container active' : 'transaction-container'}>
                                <div className="title-line">
                                    <div className="transaction-line-info">
                                        <p className="transaction-title transaction-title-timestamp inline-block">Timestamp</p>
                                        <p className="transaction-title transaction-title-walletaddr inline-block">Wallet address (sender)</p>
                                        <p className="transaction-title transaction-title-blockid inline-block">Block Identifier</p>
                                        <p className="transaction-title transaction-title-amount inline-block">Amount</p>
                                    </div>
                                </div>
                                <div id="user-transactions-container-js">
                                    <p>No transactions yet...</p>
                                </div>
                            </div>

                            <div id="search-transactions-container" className={ activeButton ==='search' ? 'transaction-container active' : 'transaction-container'}>
                                <div className="title-line">
                                    <div className="transaction-line-info">
                                        <p className="transaction-title transaction-title-timestamp inline-block">Timestamp</p>
                                        <p className="transaction-title transaction-title-walletaddr inline-block">Wallet address (sender)</p>
                                        <p className="transaction-title transaction-title-blockid inline-block">Block Identifier</p>
                                        <p className="transaction-title transaction-title-amount inline-block">Amount</p>
                                    </div>
                                </div>
                                <div id="search-transactions-container-js">
                                    <p>No transactions yet...</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="buttons">
                    </div>
                </div>
            </div>
        );
    }
}
