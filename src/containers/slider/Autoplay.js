import React from 'react';
import Slider from 'react-animated-slider';
import horizontalCss from './horizontal.css';
import content from './content';

function Autoplay() {
	return (
		<div>
			<Slider classNames={horizontalCss} autoplay={6000}>
				{content.map((item, index) => (
					<div key={index}>
						<div className="center slider-text-container-large">
							<h1 className="center item-title-large">{item.title}</h1>
							<p className="center item-desc-large">{item.description}</p>
						</div>
						<img className="slider-image-large" src={item.image} alt={item.imagealt} style={{marginTop: item.topMargin, paddingLeft: item.leftPadding}}/>
					</div>
				))}
			</Slider>

		</div>
	);
}


export default Autoplay;
