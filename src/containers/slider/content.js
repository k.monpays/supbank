export default [
	{
		title: 'Learn More About Cryptocurrency',
		description:
			'SupBank is one of the first banks to invest into cryptocurrency. Users have the possibility to mine and invest into our cryptocurrency, Plaksium, in a secured environment. Follow this path to learn more about Cryptocurrency and Blockchain.',
		button: 'Read More',
		image: 'https://www.chartiq.com/wp-content/uploads/2017/11/ChartingLibrary_Mockup.png',
		imagealt: "Computer with charts",
		topMargin: "-5px",
		leftPadding: "0",
		redirectLink: "/learn-more"
	},
	{
		title: 'Who Are We ?',
		description:
			'We are a small team that is part of Supbank. The project has been given to us as a tryout for possible future integration of cryptocurrency bank investments, and possible everyday payments.',
		button: 'Discover',
		image: 'https://www.aislelabs.com/wp-content/themes/aislelabs3/images/team.png',
		imagealt: "Team",
		topMargin: "-50px",
		leftPadding: "0",
		redirectLink: "/about-us"
	},
	{
		title: 'Get Started',
		description:
			'Join us and open a wallet today. No first payments has to be made in order to create your wallet, you can just connect, check the charts and mine.',
		button: 'Signup',
		image: 'https://www.buybitcoinworldwide.com/img/goodicons/wallet.png',
		image2: 'https://doddlenews.com/wp-content/uploads/2018/02/1280px-Blockchain_workflow.png',
		imagealt: "Wallet",
		topMargin: "-75px",
		leftPadding: "23px",
		redirectLink: "/signup"
	}
];
